﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuBehavior : MonoBehaviour
{

    public void TriggerMenuBehavior(int i)
    {
        switch (i)
        {
            default:
            case 0:
                SceneManager.LoadScene("Level");
                break;

            case 1:
                Application.Quit();
                break;
        }
    }
}
