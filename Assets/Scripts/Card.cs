﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using Assets.Scripts;

[ExecuteInEditMode]
public class Card : MonoBehaviour
{
    public static bool DO_NOT = false;

    [SerializeField]
    private CardState _state;

    [SerializeField]
    private int _cardValue;

    [SerializeField]
    private bool _initialized = false;

    private Sprite _cardBack;
    private Sprite _cardFace;

    public void SetupGraphics(GameManager manager)
    {
        _cardBack = manager.GetCardBack();
        _cardFace = manager.GetCardFace(_cardValue);
    }

    public void FlipCard()
    {
        if (DO_NOT)
        {
            return;
        }

        if (_state == CardState.ShowBack)
        {
            _state = CardState.ShowFace;
        }

        //if (_state == CardState.ShowBack && !DO_NOT)
        if (_state == CardState.ShowBack)
        {
            GetComponent<Image>().sprite = _cardBack;
        }
        //else if (_state == CardState.ShowFace && !DO_NOT)
        else if (_state == CardState.ShowFace)
        {
            GetComponent<Image>().sprite = _cardFace;

            UpdateOutline(true);
        }
    }

    private void UpdateOutline(bool visible)
    {
        Outline outline = GetComponent<Outline>();

        if (visible)
        {
            outline.effectDistance = new Vector2 { x = 1, y = -1 };
        }
        else
        {
            outline.effectDistance = new Vector2 { x = 0, y = 0 };
        }
    }

    public int CardValue
    {
        get
        {
            return _cardValue;
        }
        set
        {
            _cardValue = value;
        }
    }

    public CardState State
    {
        get
        {
            return _state;
        }
        set
        {
            _state = value;
        }
    }

    public bool Initialized
    {
        get
        {
            return _initialized;
        }
        set
        {
            _initialized = value;
        }
    }

    public void FalseCheck()
    {
        StartCoroutine(Pause());
    }

    IEnumerator Pause()
    {
        try
        {
            yield return new WaitForSeconds(1);
            if (_state == CardState.ShowBack)
            {
                GetComponent<Image>().sprite = _cardBack;
            }
            else if (_state == CardState.ShowFace)
            {
                GetComponent<Image>().sprite = _cardFace;
            }
        }
        finally
        {
            DO_NOT = false;
            UpdateOutline(false);
        }
    }
}