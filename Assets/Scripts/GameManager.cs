﻿using Assets.Scripts;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Sprite[] CardFace;
    public Sprite CardBack;
    public GameObject[] Cards;
    public Text MatchText;
    public Text TriesText;
    public Text HighScoreText;

    private bool _init = false;
    private int _matches = 0;
    private int _tries = 0;

    void Start()
    {
        if (!_init)
        {
            InitializeCards();
            InitializeHighScore();
        }
    }

    private void InitializeHighScore()
    {
        int highScore = PlayerPrefs.GetInt("highscore");

        string highScoreString = (highScore == 0) ? "N/A" : highScore.ToString();

        HighScoreText.text = "Least tries record: " + highScoreString;
    }

    void Update()
    {

        if (Input.GetMouseButtonUp(0))
        {
            CheckCards();
        }
    }

    private void InitializeCards()
    {
        for (int id = 0; id < 2; id++)
        {
            for (int i = 1; i < System.Math.Ceiling(Cards.Length / 2.0) + 1; i++)
            {
                bool test = false;
                int choice = 0;

                while (!test)
                {
                    choice = Random.Range(0, Cards.Length);

                    Card card = Cards[choice].GetComponent<Card>();

                    test = card.Initialized == false;
                }

                Cards[choice].GetComponent<Card>().CardValue = i;
                Cards[choice].GetComponent<Card>().Initialized = true;
            }
        }

        foreach (GameObject c in Cards)
        {
            c.GetComponent<Card>().SetupGraphics(this);
        }

        _matches = Cards.Length / 2;

        UpdateMatchesText();

        if (!_init)
        {
            _init = true;
        }
    }

    public Sprite GetCardBack()
    {
        return CardBack;
    }

    public Sprite GetCardFace(int i)
    {
        return CardFace[i - 1];
    }

    private void CheckCards()
    {
        List<int> c = new List<int>();

        for (int i = 0; i < Cards.Length; i++)
        {
            if (Cards[i].GetComponent<Card>().State == CardState.ShowFace)
            {
                if (!c.Contains(i))
                {
                    c.Add(i);
                }
            }
        }

        if (c.Count == 2)
        {
            _tries++;
            CardComparison(c);
        }
    }

    private void CardComparison(List<int> c)
    {
        Card.DO_NOT = true;
        CardState x = CardState.ShowBack;

        UpdateMatchesText();

        TriesText.text = "Number of tries: " + _tries;

        if (Cards[c[0]].GetComponent<Card>().CardValue == Cards[c[1]].GetComponent<Card>().CardValue)
        {
            x = CardState.Solved; // 2;
            _matches--;
            UpdateMatchesText();

            if (_matches == 0)
            {
                Card.DO_NOT = false;

                int oldHighScore = PlayerPrefs.GetInt("highscore");

                if (_tries < oldHighScore || oldHighScore == 0)
                {
                    PlayerPrefs.SetInt("highscore", _tries);
                }

                SceneManager.LoadScene("Menu");
            }
        }

        for (int i = 0; i < c.Count; i++)
        {
            Cards[c[i]].GetComponent<Card>().State = x;
            Cards[c[i]].GetComponent<Card>().FalseCheck();
        }
    }

    private void UpdateMatchesText()
    {
        MatchText.text = "Number of matches: " + _matches;
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
