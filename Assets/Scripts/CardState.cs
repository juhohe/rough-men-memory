﻿namespace Assets.Scripts
{
    public enum CardState
    {
        ShowBack,
        ShowFace,
        Solved // Means that a pair has already been found for this card.
    }
}
